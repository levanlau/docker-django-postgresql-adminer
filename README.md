# README #

Running a django project with postgresql and manage database with adminer

# Step 1:

- docker-compose run web django-admin.py startproject myproject .

- cd myproject > nano settings , add code below to DATABASES = { ... }

  'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django20',
        'USER': 'root',
		'PASSWORD': 'mypass',
        'HOST': 'db',
        'PORT': 5432,
    }
	
- docker-compose up -d (run background) or: docker-compose up (run foreground to view and bug error)

# Step 2:

- View http://localhost:8000 (django)

- View http://localhost:8080 (adminer)

# Step 3:

- docker exec -it c_web bash ("c_web" is container name)

- Inside the bash command, type: "python3.5 manage.py migrate" to create database for django, then type: "python3.5 manage.py createsuperuser" to create admin user, the last type: "exit" to exit command window.

- View http://localhost:8000/admin , login with admin user

- View http://localhost:8080, to see data of database "django20"

# Hope userful for you

